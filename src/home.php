<?php
require '../vendor/autoload.php';

use Twig\Environment;
use Twig\Loader\FilesystemLoader;

$loader = new FilesystemLoader('../template');
$twig = new Environment($loader);

echo $twig->render('home.html.twig', ['name' => 'John Doe', 
    'surname' => 'albert','matricule' => 'm124k',]);

echo $twig->render('derives.html.twig'); 
  
?>