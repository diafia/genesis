<?php return array(
    'root' => array(
        'pretty_version' => '1.0.0+no-version-set',
        'version' => '1.0.0.0',
        'type' => 'library',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'reference' => NULL,
        'name' => 'project/mit',
        'dev' => true,
    ),
    'versions' => array(
        'doctrine/inflector' => array(
            'pretty_version' => '1.4.4',
            'version' => '1.4.4.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../doctrine/inflector',
            'aliases' => array(),
            'reference' => '4bd5c1cdfcd00e9e2d8c484f79150f67e5d355d9',
            'dev_requirement' => false,
        ),
        'doctrine/lexer' => array(
            'pretty_version' => '1.2.3',
            'version' => '1.2.3.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../doctrine/lexer',
            'aliases' => array(),
            'reference' => 'c268e882d4dbdd85e36e4ad69e02dc284f89d229',
            'dev_requirement' => false,
        ),
        'dragonmantank/cron-expression' => array(
            'pretty_version' => 'v2.3.1',
            'version' => '2.3.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../dragonmantank/cron-expression',
            'aliases' => array(),
            'reference' => '65b2d8ee1f10915efb3b55597da3404f096acba2',
            'dev_requirement' => false,
        ),
        'egulias/email-validator' => array(
            'pretty_version' => '2.1.25',
            'version' => '2.1.25.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../egulias/email-validator',
            'aliases' => array(),
            'reference' => '0dbf5d78455d4d6a41d186da50adc1122ec066f4',
            'dev_requirement' => false,
        ),
        'erusev/parsedown' => array(
            'pretty_version' => '1.7.4',
            'version' => '1.7.4.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../erusev/parsedown',
            'aliases' => array(),
            'reference' => 'cb17b6477dfff935958ba01325f2e8a2bfa6dab3',
            'dev_requirement' => false,
        ),
        'illuminate/auth' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v5.8.38',
            ),
        ),
        'illuminate/broadcasting' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v5.8.38',
            ),
        ),
        'illuminate/bus' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v5.8.38',
            ),
        ),
        'illuminate/cache' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v5.8.38',
            ),
        ),
        'illuminate/config' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v5.8.38',
            ),
        ),
        'illuminate/console' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v5.8.38',
            ),
        ),
        'illuminate/container' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v5.8.38',
            ),
        ),
        'illuminate/contracts' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v5.8.38',
            ),
        ),
        'illuminate/cookie' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v5.8.38',
            ),
        ),
        'illuminate/database' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v5.8.38',
            ),
        ),
        'illuminate/encryption' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v5.8.38',
            ),
        ),
        'illuminate/events' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v5.8.38',
            ),
        ),
        'illuminate/filesystem' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v5.8.38',
            ),
        ),
        'illuminate/hashing' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v5.8.38',
            ),
        ),
        'illuminate/http' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v5.8.38',
            ),
        ),
        'illuminate/log' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v5.8.38',
            ),
        ),
        'illuminate/mail' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v5.8.38',
            ),
        ),
        'illuminate/notifications' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v5.8.38',
            ),
        ),
        'illuminate/pagination' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v5.8.38',
            ),
        ),
        'illuminate/pipeline' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v5.8.38',
            ),
        ),
        'illuminate/queue' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v5.8.38',
            ),
        ),
        'illuminate/redis' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v5.8.38',
            ),
        ),
        'illuminate/routing' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v5.8.38',
            ),
        ),
        'illuminate/session' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v5.8.38',
            ),
        ),
        'illuminate/support' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v5.8.38',
            ),
        ),
        'illuminate/translation' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v5.8.38',
            ),
        ),
        'illuminate/validation' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v5.8.38',
            ),
        ),
        'illuminate/view' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v5.8.38',
            ),
        ),
        'josrom/laravel-build-emails' => array(
            'pretty_version' => '0.1.0',
            'version' => '0.1.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../josrom/laravel-build-emails',
            'aliases' => array(),
            'reference' => 'c79f7b5217c80707f40f0d0998f36dee71e6a4a1',
            'dev_requirement' => false,
        ),
        'laravel/framework' => array(
            'pretty_version' => 'v5.8.38',
            'version' => '5.8.38.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../laravel/framework',
            'aliases' => array(),
            'reference' => '78eb4dabcc03e189620c16f436358d41d31ae11f',
            'dev_requirement' => false,
        ),
        'league/flysystem' => array(
            'pretty_version' => '1.1.9',
            'version' => '1.1.9.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../league/flysystem',
            'aliases' => array(),
            'reference' => '094defdb4a7001845300334e7c1ee2335925ef99',
            'dev_requirement' => false,
        ),
        'league/mime-type-detection' => array(
            'pretty_version' => '1.11.0',
            'version' => '1.11.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../league/mime-type-detection',
            'aliases' => array(),
            'reference' => 'ff6248ea87a9f116e78edd6002e39e5128a0d4dd',
            'dev_requirement' => false,
        ),
        'michelf/php-markdown' => array(
            'pretty_version' => '1.9.1',
            'version' => '1.9.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../michelf/php-markdown',
            'aliases' => array(),
            'reference' => '5024d623c1a057dcd2d076d25b7d270a1d0d55f3',
            'dev_requirement' => false,
        ),
        'monolog/monolog' => array(
            'pretty_version' => '1.27.0',
            'version' => '1.27.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../monolog/monolog',
            'aliases' => array(),
            'reference' => '52ebd235c1f7e0d5e1b16464b695a28335f8e44a',
            'dev_requirement' => false,
        ),
        'nesbot/carbon' => array(
            'pretty_version' => '2.57.0',
            'version' => '2.57.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../nesbot/carbon',
            'aliases' => array(),
            'reference' => '4a54375c21eea4811dbd1149fe6b246517554e78',
            'dev_requirement' => false,
        ),
        'opis/closure' => array(
            'pretty_version' => '3.6.3',
            'version' => '3.6.3.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../opis/closure',
            'aliases' => array(),
            'reference' => '3d81e4309d2a927abbe66df935f4bb60082805ad',
            'dev_requirement' => false,
        ),
        'paragonie/random_compat' => array(
            'pretty_version' => 'v9.99.100',
            'version' => '9.99.100.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../paragonie/random_compat',
            'aliases' => array(),
            'reference' => '996434e5492cb4c3edcb9168db6fbb1359ef965a',
            'dev_requirement' => false,
        ),
        'phpoption/phpoption' => array(
            'pretty_version' => '1.8.1',
            'version' => '1.8.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../phpoption/phpoption',
            'aliases' => array(),
            'reference' => 'eab7a0df01fe2344d172bff4cd6dbd3f8b84ad15',
            'dev_requirement' => false,
        ),
        'project/mit' => array(
            'pretty_version' => '1.0.0+no-version-set',
            'version' => '1.0.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'reference' => NULL,
            'dev_requirement' => false,
        ),
        'psr/container' => array(
            'pretty_version' => '1.1.1',
            'version' => '1.1.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../psr/container',
            'aliases' => array(),
            'reference' => '8622567409010282b7aeebe4bb841fe98b58dcaf',
            'dev_requirement' => false,
        ),
        'psr/event-dispatcher-implementation' => array(
            'dev_requirement' => false,
            'provided' => array(
                0 => '1.0',
            ),
        ),
        'psr/log' => array(
            'pretty_version' => '1.1.4',
            'version' => '1.1.4.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../psr/log',
            'aliases' => array(),
            'reference' => 'd49695b909c3b7628b6289db5479a1c204601f11',
            'dev_requirement' => false,
        ),
        'psr/log-implementation' => array(
            'dev_requirement' => false,
            'provided' => array(
                0 => '1.0.0',
                1 => '1.0|2.0',
            ),
        ),
        'psr/simple-cache' => array(
            'pretty_version' => '1.0.1',
            'version' => '1.0.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../psr/simple-cache',
            'aliases' => array(),
            'reference' => '408d5eafb83c57f6365a3ca330ff23aa4a5fa39b',
            'dev_requirement' => false,
        ),
        'ramsey/uuid' => array(
            'pretty_version' => '3.9.6',
            'version' => '3.9.6.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../ramsey/uuid',
            'aliases' => array(),
            'reference' => 'ffa80ab953edd85d5b6c004f96181a538aad35a3',
            'dev_requirement' => false,
        ),
        'rhumsaa/uuid' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '3.9.6',
            ),
        ),
        'swiftmailer/swiftmailer' => array(
            'pretty_version' => 'v6.3.0',
            'version' => '6.3.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../swiftmailer/swiftmailer',
            'aliases' => array(),
            'reference' => '8a5d5072dca8f48460fce2f4131fcc495eec654c',
            'dev_requirement' => false,
        ),
        'symfony/console' => array(
            'pretty_version' => 'v4.4.40',
            'version' => '4.4.40.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/console',
            'aliases' => array(),
            'reference' => 'bdcc66f3140421038f495e5b50e3ca6ffa14c773',
            'dev_requirement' => false,
        ),
        'symfony/css-selector' => array(
            'pretty_version' => 'v3.4.47',
            'version' => '3.4.47.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/css-selector',
            'aliases' => array(),
            'reference' => 'da3d9da2ce0026771f5fe64cb332158f1bd2bc33',
            'dev_requirement' => false,
        ),
        'symfony/debug' => array(
            'pretty_version' => 'v4.4.37',
            'version' => '4.4.37.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/debug',
            'aliases' => array(),
            'reference' => '5de6c6e7f52b364840e53851c126be4d71e60470',
            'dev_requirement' => false,
        ),
        'symfony/deprecation-contracts' => array(
            'pretty_version' => 'v2.5.1',
            'version' => '2.5.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/deprecation-contracts',
            'aliases' => array(),
            'reference' => 'e8b495ea28c1d97b5e0c121748d6f9b53d075c66',
            'dev_requirement' => false,
        ),
        'symfony/error-handler' => array(
            'pretty_version' => 'v4.4.40',
            'version' => '4.4.40.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/error-handler',
            'aliases' => array(),
            'reference' => '2d0c9c229d995bef5e87fe4e83b717541832b448',
            'dev_requirement' => false,
        ),
        'symfony/event-dispatcher' => array(
            'pretty_version' => 'v4.4.37',
            'version' => '4.4.37.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/event-dispatcher',
            'aliases' => array(),
            'reference' => '3ccfcfb96ecce1217d7b0875a0736976bc6e63dc',
            'dev_requirement' => false,
        ),
        'symfony/event-dispatcher-contracts' => array(
            'pretty_version' => 'v1.1.12',
            'version' => '1.1.12.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/event-dispatcher-contracts',
            'aliases' => array(),
            'reference' => '1d5cd762abaa6b2a4169d3e77610193a7157129e',
            'dev_requirement' => false,
        ),
        'symfony/event-dispatcher-implementation' => array(
            'dev_requirement' => false,
            'provided' => array(
                0 => '1.1',
            ),
        ),
        'symfony/finder' => array(
            'pretty_version' => 'v4.4.37',
            'version' => '4.4.37.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/finder',
            'aliases' => array(),
            'reference' => 'b17d76d7ed179f017aad646e858c90a2771af15d',
            'dev_requirement' => false,
        ),
        'symfony/http-client-contracts' => array(
            'pretty_version' => 'v2.5.1',
            'version' => '2.5.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/http-client-contracts',
            'aliases' => array(),
            'reference' => '1a4f708e4e87f335d1b1be6148060739152f0bd5',
            'dev_requirement' => false,
        ),
        'symfony/http-foundation' => array(
            'pretty_version' => 'v4.4.39',
            'version' => '4.4.39.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/http-foundation',
            'aliases' => array(),
            'reference' => '60e8e42a4579551e5ec887d04380e2ab9e4cc314',
            'dev_requirement' => false,
        ),
        'symfony/http-kernel' => array(
            'pretty_version' => 'v4.4.40',
            'version' => '4.4.40.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/http-kernel',
            'aliases' => array(),
            'reference' => '330a859a7ec9d7e7d82f2569b1c0700a26ffb1e3',
            'dev_requirement' => false,
        ),
        'symfony/mime' => array(
            'pretty_version' => 'v5.4.7',
            'version' => '5.4.7.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/mime',
            'aliases' => array(),
            'reference' => '92d27a34dea2e199fa9b687e3fff3a7d169b7b1c',
            'dev_requirement' => false,
        ),
        'symfony/polyfill-ctype' => array(
            'pretty_version' => 'v1.25.0',
            'version' => '1.25.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-ctype',
            'aliases' => array(),
            'reference' => '30885182c981ab175d4d034db0f6f469898070ab',
            'dev_requirement' => false,
        ),
        'symfony/polyfill-iconv' => array(
            'pretty_version' => 'v1.25.0',
            'version' => '1.25.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-iconv',
            'aliases' => array(),
            'reference' => 'f1aed619e28cb077fc83fac8c4c0383578356e40',
            'dev_requirement' => false,
        ),
        'symfony/polyfill-intl-idn' => array(
            'pretty_version' => 'v1.25.0',
            'version' => '1.25.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-intl-idn',
            'aliases' => array(),
            'reference' => '749045c69efb97c70d25d7463abba812e91f3a44',
            'dev_requirement' => false,
        ),
        'symfony/polyfill-intl-normalizer' => array(
            'pretty_version' => 'v1.25.0',
            'version' => '1.25.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-intl-normalizer',
            'aliases' => array(),
            'reference' => '8590a5f561694770bdcd3f9b5c69dde6945028e8',
            'dev_requirement' => false,
        ),
        'symfony/polyfill-mbstring' => array(
            'pretty_version' => 'v1.25.0',
            'version' => '1.25.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-mbstring',
            'aliases' => array(),
            'reference' => '0abb51d2f102e00a4eefcf46ba7fec406d245825',
            'dev_requirement' => false,
        ),
        'symfony/polyfill-php72' => array(
            'pretty_version' => 'v1.25.0',
            'version' => '1.25.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-php72',
            'aliases' => array(),
            'reference' => '9a142215a36a3888e30d0a9eeea9766764e96976',
            'dev_requirement' => false,
        ),
        'symfony/polyfill-php73' => array(
            'pretty_version' => 'v1.25.0',
            'version' => '1.25.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-php73',
            'aliases' => array(),
            'reference' => 'cc5db0e22b3cb4111010e48785a97f670b350ca5',
            'dev_requirement' => false,
        ),
        'symfony/polyfill-php80' => array(
            'pretty_version' => 'v1.25.0',
            'version' => '1.25.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-php80',
            'aliases' => array(),
            'reference' => '4407588e0d3f1f52efb65fbe92babe41f37fe50c',
            'dev_requirement' => false,
        ),
        'symfony/process' => array(
            'pretty_version' => 'v4.4.40',
            'version' => '4.4.40.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/process',
            'aliases' => array(),
            'reference' => '54e9d763759268e07eb13b921d8631fc2816206f',
            'dev_requirement' => false,
        ),
        'symfony/routing' => array(
            'pretty_version' => 'v4.4.37',
            'version' => '4.4.37.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/routing',
            'aliases' => array(),
            'reference' => '324f7f73b89cd30012575119430ccfb1dfbc24be',
            'dev_requirement' => false,
        ),
        'symfony/service-contracts' => array(
            'pretty_version' => 'v2.5.1',
            'version' => '2.5.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/service-contracts',
            'aliases' => array(),
            'reference' => '24d9dc654b83e91aa59f9d167b131bc3b5bea24c',
            'dev_requirement' => false,
        ),
        'symfony/translation' => array(
            'pretty_version' => 'v4.4.37',
            'version' => '4.4.37.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/translation',
            'aliases' => array(),
            'reference' => '4ce00d6875230b839f5feef82e51971f6c886e00',
            'dev_requirement' => false,
        ),
        'symfony/translation-contracts' => array(
            'pretty_version' => 'v2.5.1',
            'version' => '2.5.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/translation-contracts',
            'aliases' => array(),
            'reference' => '1211df0afa701e45a04253110e959d4af4ef0f07',
            'dev_requirement' => false,
        ),
        'symfony/translation-implementation' => array(
            'dev_requirement' => false,
            'provided' => array(
                0 => '1.0|2.0',
            ),
        ),
        'symfony/var-dumper' => array(
            'pretty_version' => 'v4.4.39',
            'version' => '4.4.39.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/var-dumper',
            'aliases' => array(),
            'reference' => '35237c5e5dcb6593a46a860ba5b29c1d4683d80e',
            'dev_requirement' => false,
        ),
        'tijsverkoyen/css-to-inline-styles' => array(
            'pretty_version' => '2.2.4',
            'version' => '2.2.4.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../tijsverkoyen/css-to-inline-styles',
            'aliases' => array(),
            'reference' => 'da444caae6aca7a19c0c140f68c6182e337d5b1c',
            'dev_requirement' => false,
        ),
        'twig/twig' => array(
            'pretty_version' => 'v3.3.10',
            'version' => '3.3.10.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../twig/twig',
            'aliases' => array(),
            'reference' => '8442df056c51b706793adf80a9fd363406dd3674',
            'dev_requirement' => false,
        ),
        'vlucas/phpdotenv' => array(
            'pretty_version' => 'v3.6.10',
            'version' => '3.6.10.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../vlucas/phpdotenv',
            'aliases' => array(),
            'reference' => '5b547cdb25825f10251370f57ba5d9d924e6f68e',
            'dev_requirement' => false,
        ),
    ),
);
